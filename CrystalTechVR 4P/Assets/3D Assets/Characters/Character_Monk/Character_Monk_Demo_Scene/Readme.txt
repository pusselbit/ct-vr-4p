Character "Monk" for Unity

In this asset are included:

- Animations (more to come with updates):
  Monk@Praying_Neutral
  Monk@Strafe_Left
  Monk@Strafe_Right
  Monk@Walk_Forward
  Monk@Walk_Forward_Left_Medium
  Monk@Walk_Forward_Left_Short
  Monk@Walk_Forward_Left_Wide
  Monk@Walk_Forward_Right_Medium
  Monk@Walk_Forward_Right_Short
  Monk@Walk_Forward_Right_Wide

- Source.zip file contains: 
  TPose Model with custom rig
  each animation has a separated 3ds max 2010 file

- Custom hair shader

- Custom eyes textures with material for PreIntegratedSkinShader

- Custom mouth and teeth textures with material for PreIntegratedSkinShader

What is not included:
Pre integrated Skin shader, you have to buy it separately from Unity asset store

All rights reserved to JBrothers
