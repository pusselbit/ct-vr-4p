Shader "Custom/Monk Hair Layer 2" {
	Properties {
		_Color ("Main Color", Color) = (1,1,1,1)
		_MainTex ("Base (RGB) Alpha(A)", 2D) = "white" {}
		_BaseShiftNoiseTex("Base(R) Shift(G) Noise(B)", 2D) = "white" {}
		_PrimaryShift("_PrimaryShift", Float) = 0
		_SpecularColor1("_SpecularColor1", Color) = (1, 1, 1, 1)
		_SpecExp1("_SpecExp1", Float) = 0
		_SeconaryShift("_SeconaryShift", Float) = 0
		_SpecularColor2("_SpecularColor2", Color) = (1, 1, 1, 1)
		_SpecExp2("_SpecExp2", Float) = 0
		_ShadowCasterCutoff ("_ShadowCasterCutoff", Range(0,1)) = 0.5
	}
	SubShader {
		Tags {"Queue"="Transparent-2" "IgnoreProjector"="True" "RenderType"="Transparent"  }
		LOD 400
		
		CGINCLUDE
			#include "UnityCG.cginc"
			#include "Lighting.cginc"
			fixed4 _Color;

			sampler2D _MainTex;
			sampler2D _BaseShiftNoiseTex;
			
			float _PrimaryShift;
			float _SeconaryShift;
			float _SpecExp1;
			float _SpecExp2;
			fixed3 _SpecularColor1;
			fixed3 _SpecularColor2;
			
			struct Input {
				float2 uv_MainTex;
				float2 uv_BaseShiftNoiseTex;
			};
			
			struct HairSurfaceOutput {
				fixed3 Albedo;
				fixed3 Normal;
				fixed3 Emission;
				half Specular;
				fixed Gloss;
				fixed Alpha;
				fixed Noise;
				fixed3 Tangent1;
				fixed3 Tangent2;
				fixed SGExponent1;
				fixed SGExponent2;
			};
			
			float3 ShiftTangent(float3 T, float3 N, float shift) {
				float3 shiftedT = T + shift * N;
				return normalize(shiftedT);
			}
		
			float StrandSpecular(float3 T, float3 H, float sgexponent) {
				float dotTH = dot(T, H);
				float sinTH = sqrt(1.0 - dotTH*dotTH);
				float dirAtten = smoothstep(-1.0, 0.0, dotTH);
				return dirAtten * exp2(sinTH * sgexponent - sgexponent);
			}
		
		
			inline fixed4 KajiyaKay(HairSurfaceOutput s, fixed3 lightDir, half3 halfDir, fixed atten) {
				half3 H = halfDir;
				float NdotL = dot(s.Normal, lightDir);
				float3 diffuse = saturate(lerp(0.25, 1.0, NdotL));
				
				// first specular highlight
				float3 t1 = s.Tangent1;
				float specularValue1 = StrandSpecular(s.Tangent1, H, s.SGExponent1);
				float3 specular1 = _SpecularColor1.rgb * specularValue1;
	
				// second specular highlight
				float3 t2 = s.Tangent2;
				float specularValue2 = StrandSpecular(s.Tangent2, H, s.SGExponent2);
				float3 specular2 = s.Noise * _SpecularColor2.rgb * specularValue2;
				
				// limit specular to lit to avoid shining when there are no shadows
				float specMask = max(0,NdotL)  + 0.0000000000000001;
				specular1 *= specMask;
				specular2 *= specMask;
				
				fixed4 c;
				c.rgb = (diffuse * s.Albedo + specular1 + specular2)  * _LightColor0.rgb * (2*atten) ;
				c.a = s.Alpha;
				return c;
			}
			


			void surfHair(Input IN, inout HairSurfaceOutput o) {
				
				float4 main = tex2D(_MainTex, IN.uv_MainTex) * _Color;
				fixed3 base_shift_noise = tex2D(_BaseShiftNoiseTex, IN.uv_BaseShiftNoiseTex).rgb;
				o.Albedo = base_shift_noise.r * _Color.rgb * main.rgb;
				float shift = base_shift_noise.g;
				o.Noise = base_shift_noise.b;
				o.Alpha = main.a;
				
				const float3 verticalNormal = float3(0,0,1);
				const float3 verticalTangent = float3(0,1,0);
				
				o.Gloss = 0;
				o.Specular = 0;
				o.Normal = verticalNormal;
							
				float shift1 = _PrimaryShift + shift;
				float shift2 = _SeconaryShift + shift;
				o.Tangent1 = normalize(float3(0,1,shift1));
				o.Tangent2 = normalize(float3(0,1,shift1));
				o.SGExponent1 = _SpecExp1 * 1.4427f + 1.4427f;
				o.SGExponent2 = _SpecExp2 * 1.4427f + 1.4427f;
				o.Emission = 0;				
			}
			
			void surfHairBack(Input IN, inout HairSurfaceOutput o) {
				
				float4 main = tex2D(_MainTex, IN.uv_MainTex) * _Color;
				fixed3 base_shift_noise = tex2D(_BaseShiftNoiseTex, IN.uv_BaseShiftNoiseTex).rgb;
				o.Albedo = base_shift_noise.r * _Color.rgb * main.rgb;
				float shift = base_shift_noise.g;
				o.Noise = base_shift_noise.b;
				o.Alpha = main.a;
				
				const float3 verticalNormal = -float3(0,0,1);
				const float3 verticalTangent = float3(0,1,0);
				
				o.Gloss = 0;
				o.Specular = 0;
				o.Normal = verticalNormal;
							
				float shift1 = _PrimaryShift + shift;
				float shift2 = _SeconaryShift + shift;
				o.Tangent1 = normalize(float3(0,1,-shift1));
				o.Tangent2 = normalize(float3(0,1,-shift1));
				o.SGExponent1 = _SpecExp1 * 1.4427f + 1.4427f;
				o.SGExponent2 = _SpecExp2 * 1.4427f + 1.4427f;
				o.Emission = 0;				
			}
		ENDCG

		ZWrite Off
		ZTest LEqual
		Blend Off

		Cull Front
		CGPROGRAM
			#pragma surface surfHairBack HairBack alpha halfasview  
			#pragma target 2.0
			
			inline fixed4 LightingHairBack(HairSurfaceOutput s, fixed3 lightDir, half3 viewDir, fixed atten) {
				return KajiyaKay(s, lightDir, viewDir, atten);
			}
		ENDCG
		
		Cull Back
		CGPROGRAM
			#pragma surface surfHair HairFront alpha halfasview  
			#pragma target 2.0
			inline fixed4 LightingHairFront(HairSurfaceOutput s, fixed3 lightDir, half3 viewDir, fixed atten) {
				return KajiyaKay(s, lightDir, viewDir, atten);
			}
			
			
		ENDCG
		
		Pass {
			Name "Caster"
			Tags { "LightMode" = "ShadowCaster" }
			Offset 1, 1
			
			Fog {Mode Off}
			ZWrite On ZTest LEqual Cull Off
			
			CGPROGRAM
				#pragma vertex vert
				#pragma fragment frag
				#pragma multi_compile_shadowcaster
				#pragma fragmentoption ARB_precision_hint_fastest
				#include "UnityCG.cginc"

				struct v2f { 
					V2F_SHADOW_CASTER;
					float2  uv : TEXCOORD1;
				};
				
//				sampler2D _MainTex;
				uniform float4 _MainTex_ST;
				float _ShadowCasterCutoff;
			
				v2f vert(appdata_base v) {
					v2f o;
					TRANSFER_SHADOW_CASTER(o)
					o.uv = TRANSFORM_TEX(v.texcoord, _MainTex);
					return o;
				}
				
				
				float4 frag( v2f i ) : COLOR {
					fixed alpha = tex2D(_MainTex, i.uv).a;
					clip( alpha - _ShadowCasterCutoff );
					
					SHADOW_CASTER_FRAGMENT(i)
				}
			ENDCG
		}
	}
	

	
//	FallBack "Transparent/Diffuse"
	FallBack Off
}
