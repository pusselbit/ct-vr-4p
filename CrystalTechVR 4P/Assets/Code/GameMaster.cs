﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameMaster : MonoBehaviour
{
	public static GameMaster main;
	public List<Participant> participants;

	public int currentPlayerNumber;

	public Material normalSkyboxMaterial;
	public AudioSource normalMusic;

	[Header("Tarot Cards")]
	public List<TarotCard> allTarotCards;
	public List<TarotCard> currentTarotDeck;
	public List<Transform> tarotCardSpawnPositions;

	[Header("Colors")]
	public List<Color> allColorPresets;
	public List<Color> currentColorsRemaining;
	public List<MeshRenderer> colorObjects;
	public float colorIntensity;
	public float colorLerpSpeed;

	public bool noColorDuplicates;

	[Range(0f, 1f)]
	public float colorLerpPercent;
	public bool startColorLerp = false;

	public Color colorObjectDefaultColor;
	public Color currentRandomizedColor;

	[Header("Spirit Animals")]
	public List<SpiritAnimal> allSpiritAnimals;
	public List<SpiritAnimal> currentSpiritAnimalsRemaining;

	[Header("Spirit Guides")]
	public List<Transform> spiritGuideSpawnPositions;
	public List<SpiritGuide> allSpiritGuides;
	public List<SpiritGuide> currentSpiritGuideList;
	public List<GameObject> spiritGuideActivatedParticleFx;

	public Color randomizedColor;

	[Header("Evil Presence")]
	public GameObject evilParticleFXParent;

	public Transform evilTransformSpawnParent;

	public AudioClip startAudio;
	public AudioClip loopingAudio;
	public AudioClip endAudio;

	public Material evilSkyboxMaterial;

	public List<GameObject> evilPresenceSparks;

	public GameObject evilPresenceHeart;
	public GameObject evilPresenceCloak;

	public bool activateEvilEnvironment = true;

	public Animator evilAnim;

	public bool evilHasBeenExorcised = false;

	[Header("Final Exorcism")]
	public bool canExorciseEvil;

	public bool player1Activated;
	public bool player2Activated;
	public bool player3Activated;
	public bool player4Activated;

	public GameObject p1_toP2_energy;
	public GameObject p1_toP4_energy;
	public GameObject p3_toP2_energy;
	public GameObject p3_toP4_energy;

	public GameObject centralLightning;
	public GameObject voidPortal;

	public GameObject lastFlutter;


	// Start is called before the first frame update
	void Start()
	{
		main = this;
		
		currentTarotDeck = allTarotCards;
		currentColorsRemaining = allColorPresets;
		currentSpiritAnimalsRemaining = allSpiritAnimals;
		currentSpiritGuideList = allSpiritGuides;

		evilAnim = evilPresenceHeart.GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
		if (canExorciseEvil)
		{
			if(player1Activated && player2Activated)
			{
				p1_toP2_energy.SetActive(true);
			}
			else
			{
				p1_toP2_energy.SetActive(false);
			}

			if (player2Activated && player3Activated)
			{
				p3_toP2_energy.SetActive(true);
			}
			else
			{
				p3_toP2_energy.SetActive(false);
			}

			if (player3Activated && player4Activated)
			{
				p3_toP4_energy.SetActive(true);
			}
			else
			{
				p3_toP4_energy.SetActive(false);
			}

			if (player4Activated && player1Activated)
			{
				p1_toP4_energy.SetActive(true);
			}
			else
			{
				p1_toP4_energy.SetActive(false);
			}

			if(player1Activated && player2Activated && player3Activated && player4Activated && !evilHasBeenExorcised)
			{
				StartCoroutine(ExorciseEvil());
				//ExorciseEvil();
			}
		}

		/*
		if (Input.GetKeyDown(KeyCode.Alpha1))
		{
			DrawTarotCardForPlayer(0);
			DisplayPlayerColor(0);
			AssignSpiritGuideToPlayer(0);
		}

		if (Input.GetKeyDown(KeyCode.Alpha2))
		{
			DrawTarotCardForPlayer(1);
			DisplayPlayerColor(1);
		}
		if (Input.GetKeyDown(KeyCode.Alpha3))
		{
			DrawTarotCardForPlayer(2);
			DisplayPlayerColor(2);
		}
		if (Input.GetKeyDown(KeyCode.Alpha4))
		{
			DrawTarotCardForPlayer(3);
			DisplayPlayerColor(3);
		}
		*/

		if (startColorLerp && colorLerpPercent<1.0)
		{
			colorLerpPercent += Time.deltaTime * colorLerpSpeed;
		}
		else
		{
			//colorLerpPercent = 0;
		}

		colorObjects[currentPlayerNumber].material.SetColor("_EmissionColor", Color.Lerp(colorObjectDefaultColor, currentRandomizedColor, colorLerpPercent));

		if (Input.GetKey(KeyCode.Alpha1))
		{
			player1Activated = true;
		}
		else
		{
			//player1Activated = false;
		}

		if (Input.GetKey(KeyCode.Alpha2))
		{
			player2Activated = true;
		}
		else
		{
			//player2Activated = false;
		}

		if (Input.GetKey(KeyCode.Alpha3))
		{
			player3Activated = true;
		}
		else
		{
			//player3Activated = false;
		}

		if (Input.GetKey(KeyCode.Alpha4))
		{
			player4Activated = true;
		}
		else
		{
			//player4Activated = false;
		}
	}

	public void DrawTarotCardForPlayer(int playerIndex)
	{
		int randomNumber = Random.Range(0, currentTarotDeck.Count);
		TarotCard drawnCard = currentTarotDeck[randomNumber];

		Debug.Log("Number: " + randomNumber + ", Drawn Card: " + drawnCard.cardName);

		GameObject cardCopy = Instantiate(drawnCard.cardPrefab, tarotCardSpawnPositions[playerIndex]);
	}

	public void DisplayPlayerColor(int playerIndex)
	{
		currentPlayerNumber = playerIndex;

		colorObjectDefaultColor = colorObjects[playerIndex].material.GetColor("_EmissionColor");

		currentRandomizedColor = SelectRandomColor() * colorIntensity;

		colorLerpPercent = 0.0f;
		startColorLerp = true;

		colorObjects[playerIndex].GetComponent<AudioSource>().Play();
	}

	public Color SelectRandomColor()
	{
		int randomNumber = Random.Range(0, currentColorsRemaining.Count);
		Color selectedColor = currentColorsRemaining[randomNumber];

		if (noColorDuplicates)
		{
			//TODO: Send over internet which index to remove
			currentColorsRemaining.RemoveAt(randomNumber);
		}
		return selectedColor;
	}

	public void RemoveSpawnedColor(int indexToRemove)
	{
		currentColorsRemaining.RemoveAt(indexToRemove);
	}

	public void SelectSpiritAnimalForPlayer(int playerIndex)
	{
		int randomNo = Random.Range(0, currentSpiritAnimalsRemaining.Count);

		SpiritAnimal chosenAnimal = currentSpiritAnimalsRemaining[randomNo];

		AudioController.instance.mainAudio.PlayOneShot(chosenAnimal.sound1);
	}

	public void AssignSpiritGuideToPlayer(int playerIndex)
	{
		int randomNo = Random.Range(0, currentSpiritGuideList.Count);

		SpiritGuide thisPlayersGuide = currentSpiritGuideList[randomNo];
		
		currentSpiritGuideList.RemoveAt(randomNo);

		GameObject guideCopy = Instantiate(thisPlayersGuide.guidePrefab, spiritGuideSpawnPositions[playerIndex]);

		AudioController.instance.mainAudio.PlayOneShot(thisPlayersGuide.greetingSound);
	}

	public void SpawnEvilRift(int versionNo)
	{
		GameObject sparkCopy = Instantiate(evilPresenceSparks[versionNo], evilTransformSpawnParent);
		normalMusic.Stop();
	}

	public void ActivateEvilPresencePhase1()
	{
		evilParticleFXParent.SetActive(true);

		canExorciseEvil = true;

		evilPresenceHeart.SetActive(true);

		if (activateEvilEnvironment)
		{
			StartCoroutine(ActivateEvilEnvironment());
		}
	}

	IEnumerator ActivateEvilEnvironment()
	{
		yield return new WaitForSeconds(3.0f);

		RenderSettings.skybox = evilSkyboxMaterial;

		evilPresenceCloak.SetActive(true);
	}

	IEnumerator ExorciseEvil()
	{
		voidPortal.SetActive(true);
		centralLightning.SetActive(true);

		evilAnim.SetTrigger("Exorcism_Complete");

		yield return new WaitForSeconds(13.0f);

		Destroy(evilPresenceHeart);
		Destroy(evilPresenceCloak);

		centralLightning.SetActive(false);
		voidPortal.SetActive(false);

		lastFlutter.SetActive(true);
		evilHasBeenExorcised = true;

		RenderSettings.skybox = normalSkyboxMaterial;

		normalMusic.Play();

		canExorciseEvil = false;
		
		p1_toP2_energy.SetActive(false);
		p1_toP4_energy.SetActive(false);
		p3_toP2_energy.SetActive(false);
		p3_toP4_energy.SetActive(false);
	}
}
