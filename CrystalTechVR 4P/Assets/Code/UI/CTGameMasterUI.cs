﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CTGameMasterUI : MonoBehaviour
{
    public EventTriggerButton EventTriggerButtonPrefab; 
    
    private List<EventTriggerButton> _buttonInstances = new List<EventTriggerButton>();
    
    public void Init(List<CTEventManager.Event> events)
    {
        foreach (var e in events)
        {
            var newButton = Instantiate(EventTriggerButtonPrefab, EventTriggerButtonPrefab.transform.parent)
                .GetComponent<EventTriggerButton>();
            
            newButton.Init(e);
            _buttonInstances.Add(newButton);
        }
        
        EventTriggerButtonPrefab.gameObject.SetActive(false);
    }
}
