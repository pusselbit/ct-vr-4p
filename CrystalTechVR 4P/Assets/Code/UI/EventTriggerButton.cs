﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EventTriggerButton : MonoBehaviour
{
    public Text EventIDText;
    public Text DescriptionText;
    public Text ShortCutText;
    public Text PlayerTriggerTypeText; 
    
    private CTEventManager.Event _event; 
    
    public void Init(CTEventManager.Event e)
    {
        EventIDText.text = e.ID;
        DescriptionText.text = e.Description;
        if (e.KeyboardShortcut == KeyCode.None) ShortCutText.gameObject.SetActive(false);
        else ShortCutText.text = e.KeyboardShortcut.ToString().Replace("KeyCode.", "");
        PlayerTriggerTypeText.text = e.TriggerType.ToString().Replace("CTEventManager.PlayerTriggerType.", "");
        _event = e;
    }

    public void OnClick()
    {
        CTEventManager.Instance.TriggerEventFromControl(_event.ID);
    }
}
