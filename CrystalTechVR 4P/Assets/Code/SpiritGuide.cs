﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Tarot Card", menuName = "CrystalTech/Create New Spirit Guide", order = 3)]
public class SpiritGuide : ScriptableObject
{
	public string guideName;

	public GameObject guidePrefab;

	public AudioClip greetingSound;
	public AudioClip interactSound;
	public AudioClip affirmationSound;
}
