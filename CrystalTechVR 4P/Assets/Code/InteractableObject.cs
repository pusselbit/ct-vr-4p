﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Valve.VR;

public class InteractableObject : MonoBehaviour, IInteractable {

	public bool isLookedAt;

	public bool isActivated;

	public string interactionDescText;
	public Text objectText;

	public List<GameObject> objectsToToggle;

	public bool isDisplayingInfo;
	public bool isInRightHand;

	public virtual void OnLook()
	{
		isLookedAt = true;
	}

	public SteamVR_Input_Sources HandHoldingThisObject()
	{
		SteamVR_Input_Sources handHoldingMe;

		if (isInRightHand)
		{
			handHoldingMe = SteamVR_Input_Sources.RightHand;
		}
		else
		{
			handHoldingMe = SteamVR_Input_Sources.LeftHand;
		}

		return handHoldingMe;
	}

	public virtual void OffLook()
	{
		isLookedAt = false;
	}

	public virtual void OnActivate()
	{
		foreach(GameObject go in objectsToToggle)
		{
			go.SetActive(true);
		}

		isActivated = true;

		Debug.Log("OnActivate!");
	}

	public virtual void OnDeactivate()
	{
		foreach (GameObject go in objectsToToggle)
		{
			go.SetActive(false);
		}

		isActivated = false;
	}

	public void OnTouch()
	{
		throw new System.NotImplementedException();
	}

	public void OffTouch()
	{
		throw new System.NotImplementedException();
	}
}
