﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IInteractable {
	void OnLook();
	void OffLook();

	void OnTouch();

	void OffTouch();
	void OnActivate();
	void OnDeactivate();
}
