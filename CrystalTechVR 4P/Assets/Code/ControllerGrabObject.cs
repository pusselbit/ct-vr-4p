﻿using System.Collections;
using System.Collections.Generic;
using Photon.Pun;
using UnityEngine;
using Valve.VR;

public class ControllerGrabObject : MonoBehaviour
{
	public SteamVR_Input_Sources handType;
	public SteamVR_Behaviour_Pose controllerPose;
	public SteamVR_Action_Boolean grabAction;

	[SerializeField]
	private GameObject collidingObject;

	public GameObject objectInHand;

	public InteractableObject currentCollidingInteractableObject;

	private void Update()
	{
		if (grabAction.GetLastStateDown(handType)) //If this hand's controller used the "Grab" action
		{
			Debug.Log("Trying to grab!");

			if (collidingObject && collidingObject.GetComponent<Rigidbody>()) //If there is an object that is possible to grab, trigger the Grab method and grab it
			{
				GrabObject();
			}
		}

		if (grabAction.GetLastStateUp(handType)) //If this hand's controller released the "Grab" action button
		{
			if (objectInHand) //If the controller is grabbing an object, trigger the Release Object method
			{
				ReleaseObject();
			}
		}
	}

	private void SetCollidingObject(Collider col)
	{
		if (collidingObject != null) 
			//If the Hand is already collding with another object, or if the Colliding Object does NOT have a RB, do nothing. 
		{
			return;
		}

		collidingObject = col.gameObject;
		Debug.Log("Setting Colliding GameObject: " + col.name);
	}

	void OnTriggerEnter(Collider other)
	{
		Debug.Log("Trigger Enter!" + other.name);
		SetCollidingObject(other);
	}
	
	void OnTriggerStay(Collider other)
	{
		SetCollidingObject(other);
	}

	void OnTriggerExit(Collider other)
	{
		if (!collidingObject)
		{
			return;
		}

		collidingObject = null;
		currentCollidingInteractableObject = null;
	}

	public bool IsLeftHand()
	{
		bool isLeftHand;

		if(handType == SteamVR_Input_Sources.LeftHand)
		{
			isLeftHand = true;
		}
		else
		{
			isLeftHand = false;
		}

		return isLeftHand;
	}

	public bool IsRightHand()
	{
		bool isRightHand;

		if (handType == SteamVR_Input_Sources.RightHand)
		{
			isRightHand = true;
		}
		else
		{
			isRightHand = false;
		}

		return isRightHand;
	}

	private void GrabObject()
	{
		if (collidingObject == null || collidingObject.GetComponent<PhotonView>() == null || collidingObject.GetComponent<PhotonView>().OwnershipTransfer != OwnershipOption.Takeover)
		{
			Debug.LogError("Trying to grab non-synced object");
			return;
		}
		
		collidingObject.GetComponent<PhotonView>().TransferOwnership(PhotonNetwork.LocalPlayer);
		objectInHand = collidingObject;
		collidingObject = null;
		
		var joint = AddFixedJoint();
		joint.connectedBody = objectInHand.GetComponent<Rigidbody>();

		if (objectInHand.GetComponent<InteractableObject>())
		{
			currentCollidingInteractableObject = objectInHand.GetComponentInChildren<InteractableObject>();

			if (IsRightHand())
			{
				currentCollidingInteractableObject.isInRightHand = true;
			}

			if (IsLeftHand())
			{
				currentCollidingInteractableObject.isInRightHand = false;
			}
		}
	}

	private FixedJoint AddFixedJoint()
	{
		FixedJoint fx = gameObject.AddComponent<FixedJoint>();
		fx.breakForce = 50000;
		fx.breakTorque = 50000;
		return fx;
	}

	private void ReleaseObject()
	{
		Debug.Log("Release!");

		if (GetComponent<FixedJoint>())
		{
			GetComponent<FixedJoint>().connectedBody = null;
			Destroy(GetComponent<FixedJoint>());

			objectInHand.GetComponent<Rigidbody>().velocity = controllerPose.GetVelocity();
			objectInHand.GetComponent<Rigidbody>().angularVelocity = controllerPose.GetAngularVelocity();
		}

		objectInHand = null;
	}

}
