﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu (fileName = "New Tarot Card", menuName = "CrystalTech/Create New Tarot Card", order = 1)]
public class TarotCard : ScriptableObject
{
	public string cardName;
	public Sprite cardImage;
	[TextArea(3, 8)]
	public string cardDescription;
	public int cardIdNo;

	public GameObject cardPrefab;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
