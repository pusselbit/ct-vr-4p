﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Participant : MonoBehaviour
{
	public int playerNo;

	public string participantName;

	public bool hasRecievedTarotMessage;
	public bool hasRecievedColorMessage;
	public bool hasRecievedAnimalMessage;
	public bool hasRecievedSpiritGuideMessage;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
