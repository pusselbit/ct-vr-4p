﻿using System;
using System.Collections;
using System.Collections.Generic;
using Photon.Pun;
using UnityEngine;

public class ToggleBasedOnOwnership : MonoBehaviour
{
    public PhotonView PhotonView;
    public bool EnableOnOwned; 
    public GameObject ToToggle; 
    
    void Update()
    {
        ToToggle.SetActive(PhotonView.IsMine == EnableOnOwned);
    }
}
