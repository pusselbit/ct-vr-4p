﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using Photon.Pun;
using UnityEngine;
using Valve.VR;

//Add this script to multiplayer scene
public class GameSetupController : MonoBehaviour
{
    public Transform Head, LeftController, RightController; 
    
    public static int PlayerIndex;
    public Transform VROrigin; 
    
    public List<InstantiationParams> PlayerStartParams; 
    
    void Start()
    {
        VROrigin.position = PlayerStartParams[PlayerIndex].StartPosition.position;
        VROrigin.rotation = PlayerStartParams[PlayerIndex].StartPosition.rotation; 
        CreatePlayer();
    }

    private void CreatePlayer()
    {
        var param = PlayerStartParams[PlayerIndex];
        var head = PhotonNetwork.Instantiate(param.Head.name, Vector3.zero, Quaternion.identity);
        var left = PhotonNetwork.Instantiate(param.LeftController.name, Vector3.zero, Quaternion.identity);
        var right = PhotonNetwork.Instantiate(param.RightController.name, Vector3.zero, Quaternion.identity);
        var relay = PhotonNetwork.Instantiate("PlayerEventRelay", Vector3.zero, Quaternion.identity).GetComponent<PlayerEventRelay>();
        relay.SetPlayerIndex(PlayerIndex); 
        head.GetComponent<FollowTransform>().Target = Head;
        left.GetComponent<FollowTransform>().Target = LeftController;
        right.GetComponent<FollowTransform>().Target = RightController;

        if (left.GetComponent<SteamVR_Behaviour_Skeleton>() != null)
        {
            Debug.Log("Found VR Skeleton in Left Hand Prefab!");
            SteamVR_Behaviour_Skeleton leftHandSkeleton = left.GetComponent<SteamVR_Behaviour_Skeleton>();
            leftHandSkeleton.origin = VROrigin;
        }
        
        if (right.GetComponent<SteamVR_Behaviour_Skeleton>() != null)
        {
            Debug.Log("Found VR Skeleton in Left Hand Prefab!");
            SteamVR_Behaviour_Skeleton rightHandSkeleton = right.GetComponent<SteamVR_Behaviour_Skeleton>();
            rightHandSkeleton.origin = VROrigin;
        }
    }
    
    [System.Serializable]
    public class InstantiationParams
    {
        public Transform StartPosition;
        public GameObject LeftController;
        public GameObject RightController;
        public GameObject Head;
    }
}
