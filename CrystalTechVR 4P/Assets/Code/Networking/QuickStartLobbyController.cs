﻿using System.Collections;
using System.Collections.Generic;
using Photon.Pun;
using Photon.Realtime;
using UnityEngine;
using UnityEngine.UI;

public class QuickStartLobbyController : MonoBehaviourPunCallbacks
{
    [SerializeField] private GameObject quickStartButton;
    [SerializeField] private GameObject quickCancelButton;
    [SerializeField] private int RoomSize = 4;
    [SerializeField] private Dropdown PlayerIndexDropdown;

    void Start()
    {
        GameSetupController.PlayerIndex = PlayerPrefs.GetInt(PLAYER_INDEX_REF, 0);
        PlayerIndexDropdown.SetValueWithoutNotify(GameSetupController.PlayerIndex);
    }

    public override void OnConnectedToMaster()
    {
        PhotonNetwork.AutomaticallySyncScene = true; 
        base.OnConnectedToMaster();
    }

    private const string PLAYER_INDEX_REF = "player_index";
    public void SetPlayerIndex(int index)
    {
        GameSetupController.PlayerIndex = index;
        PlayerPrefs.SetInt(PLAYER_INDEX_REF, index);
    }
    
    public void QuickStart()
    {
        quickStartButton.SetActive(false);
        quickCancelButton.SetActive(true);
        PhotonNetwork.JoinRandomRoom();
        Debug.Log("Quick start");
    }

    public override void OnJoinRandomFailed(short returnCode, string message)
    {
        CreateRoom();
    }

    private void CreateRoom()
    {
        int randomRoomNumber = Random.Range(0, 10000);
        RoomOptions roomOps = new RoomOptions {IsVisible = true, IsOpen = true, MaxPlayers = (byte) RoomSize};
        PhotonNetwork.CreateRoom("Room_" + randomRoomNumber, roomOps);
        Debug.Log("Room: " + randomRoomNumber);
    }

    public override void OnCreateRoomFailed(short returnCode, string message)
    {
        CreateRoom();
    }

    public void QuickCancel()
    {
        quickCancelButton.SetActive(false);
        quickStartButton.SetActive(true);
        PhotonNetwork.LeaveRoom();
    }
}
