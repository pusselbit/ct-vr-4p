﻿using System;
using System.Collections;
using System.Collections.Generic;
using Photon.Pun;
using UnityEngine;

public class PlayerEventRelay : PhotonBehaviour
{
    private CTEventManager _eventManager;
    [NonSerialized] public int LocalPlayerIndex = -1; //-1 means non-local player  
    
    void Awake()
    {
        _eventManager = FindObjectOfType<CTEventManager>();
        _eventManager.Register(this);
    }

    public void SetPlayerIndex(int index)
    {
        PhotonView.RPC("RPCSetIndex", RpcTarget.All, index);
    }

    [PunRPC]
    public void RPCSetIndex(int index)
    {
        LocalPlayerIndex = index; 
    }

    [PunRPC]
    public void RPCTrigger(string eventID)
    {
        CTEventManager.Instance.FinallyExecuteEventFromServer(eventID);
    }

    public void Trigger(string eventID)
    {
        PhotonView.RPC("RPCTrigger", RpcTarget.All, eventID);
    }
}