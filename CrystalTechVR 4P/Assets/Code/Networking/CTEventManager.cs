﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Serialization;

public class CTEventManager : MonoBehaviour
{
    public static CTEventManager Instance { get; private set; }
    
    public CTGameMasterUI UI; 
    public List<Event> Events;

    private List<PlayerEventRelay> _relays = new List<PlayerEventRelay>();

    void Start()
    {
        Instance = this;
        if (UI == null) UI = FindObjectOfType<CTGameMasterUI>();

        foreach (var remoteTriggerObj in FindObjectsOfType<SyncedEventTriggerObject>())
        {
            Events.AddRange(remoteTriggerObj.Events);
        }

        foreach (var e in Events)
        {
            if(Events.Count(ee => ee.ID == e.ID) > 1) 
                throw new Exception("More than one event with the same ID found.");
            if(Events.Count(ee => ee.KeyboardShortcut != KeyCode.None && ee.KeyboardShortcut == e.KeyboardShortcut) > 1)
                throw new Exception("More than one event use keycode: " + e.KeyboardShortcut);
        }
        
        UI.Init(Events);
    }
    
    public void TriggerEventFromControl(string eventID)
    {
        Event e = GetEventFromID(eventID);
        TriggerEventFromControl(e);
    }

    private void TriggerEventFromControl(Event e)
    {
        foreach (var relay in _relays)
        {
            Debug.Log("RELAY: " + relay.name);
            if (relay.LocalPlayerIndex == GameSetupController.PlayerIndex)
                relay.Trigger(e.ID);
        }
    }

    public void FinallyExecuteEventFromServer(string eventID)
    {
        Event e = GetEventFromID(eventID);
        if(e.TriggerType == PlayerTriggerType.AllPlayers || (int)e.TriggerType == GameSetupController.PlayerIndex) 
            e.Target.Invoke();
    }

    private Event GetEventFromID(string eventId)
    {
        foreach (var e in Events)
        {
            if (e.ID == eventId)
                return e;
        }

        return null;
    }

    public enum PlayerTriggerType
    {
        AllPlayers = 100,
        Player1 = 0,
        Player2 = 1,
        Player3 = 2,
        Player4 = 3
    }

    public void Register(PlayerEventRelay playerEventRelay)
    {
        _relays.Add(playerEventRelay);
    }
    
    [Serializable]
    public class Event
    {
        public string ID;
        public PlayerTriggerType TriggerType = PlayerTriggerType.AllPlayers;
        public KeyCode KeyboardShortcut = KeyCode.None;
        public string Description; 
        public UnityEvent Target;
    }
}