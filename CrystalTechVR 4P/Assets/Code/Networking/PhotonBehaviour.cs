﻿using System;
using System.Collections;
using System.Collections.Generic;
using Photon.Pun;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Serialization;

[RequireComponent(typeof(PhotonView))]
public class PhotonBehaviour : MonoBehaviour
{
    private PhotonView _photonView;

    public PhotonView PhotonView 
    {
        get
        {
            if (_photonView == null) _photonView = GetComponent<PhotonView>();
            return _photonView;
        }
    }
    //
    // public void TriggerEvent(string eventID)
    // {
    //     PhotonView.RPC("RPC_Event", RpcTarget.All, eventID);
    // }

    // [PunRPC]
    // public void RPC_Event(string eventID)
    // {
    //     foreach (var ev in Events)
    //     {
    //         if (ev.EventID == eventID)
    //         {
    //             ev.OnTrigger.Invoke();
    //         }
    //     }
    // }
    //
    [Serializable]
    public class Event
    {
        public string EventID;
        public UnityEvent OnTrigger; 
    }
}
