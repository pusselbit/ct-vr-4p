﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowTransform : MonoBehaviour
{
    [NonSerialized]
    public Transform Target;
    void Update()
    {
        if(Target == null) return;
        transform.position = Vector3.Lerp(transform.position, Target.transform.position, Time.deltaTime * 45);
        transform.rotation = Quaternion.Slerp(transform.rotation, Target.rotation, Time.deltaTime * 45);
    }
}
