﻿using System;
using System.Collections;
using System.Collections.Generic;
using Photon.Pun;
using UnityEngine;

public class SyncedObject : PhotonBehaviour, IPunObservable
{
    public GameObject TargetObject;
    
    private float _syncDistance;
    private float _syncAngleDistance;
    private Vector3 _targetPosition;
    private Quaternion _targetRotation;
    private Vector3 _prevPos;
    private Vector3 _dir;
    
    private void Start()
    {
        if (TargetObject == null) TargetObject = gameObject;
    }

    private void Update()
    {
        if (PhotonView.IsMine)
        {
            
        }
        else
        {
            TargetObject.transform.position = Vector3.MoveTowards(TargetObject.transform.position, _targetPosition,
                _syncDistance * (1.0f / PhotonNetwork.SerializationRate));
            TargetObject.transform.rotation = Quaternion.RotateTowards(TargetObject.transform.rotation, _targetRotation,
                _syncAngleDistance * (1.0f / PhotonNetwork.SerializationRate));
        }
    }

    private Vector3 _followPos; 
    public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
        if (stream.IsReading)
        {
            var pos = Vector3.zero;
            var rot = Quaternion.identity;
            stream.Serialize(ref pos);
            stream.Serialize(ref rot);
            stream.Serialize(ref _dir);
            _targetPosition = pos;
            _targetRotation = rot;
            
            // if(!_disabled)
            // {
                float lag = Mathf.Abs((float) (PhotonNetwork.Time - info.SentServerTime));
                _targetPosition += _dir * lag;
                _syncDistance = Vector3.Distance(TargetObject.transform.position, _targetPosition);
                _syncAngleDistance = Quaternion.Angle(TargetObject.transform.rotation, _targetRotation);
            // }
        }
        else if (stream.IsWriting)
        {
            // if (_disabled)
            // {
            //     var a = Vector3.zero;
            //     var b = Quaternion.identity;
            //     var c = Vector3.zero;
            //     _prevPos = _followTransform.position;
            //     stream.Serialize(ref a);
            //     stream.Serialize(ref b);
            //     stream.Serialize(ref c);
            // }

            var position = (_followPos == Vector3.zero || Vector3.Distance(_followPos, TargetObject.transform.position) > 0.03) ? TargetObject.transform.position : _followPos;
            var pos = position;
            var rot = TargetObject.transform.rotation;
            var dir = position - _prevPos;
            _prevPos = position;
            stream.Serialize(ref pos);
            stream.Serialize(ref rot);
            stream.Serialize(ref dir);
            _followPos = position; 
        }
    }
}
