﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControllerTriggerCollider : MonoBehaviour
{
    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "TriggerVolume1")
        {
            GameMaster.main.player1Activated = true;
        }

        if (other.gameObject.tag == "TriggerVolume2")
        {
            GameMaster.main.player2Activated = true;
        }


        if (other.gameObject.tag == "TriggerVolume3")
        {
            GameMaster.main.player3Activated = true;
        }

        if (other.gameObject.tag == "TriggerVolume4")
        {
            GameMaster.main.player4Activated = true;
        }
    }

    void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "TriggerVolume1")
        {
            GameMaster.main.player1Activated = false;
        }
        
        if (other.gameObject.tag == "TriggerVolume2")
        {
            GameMaster.main.player2Activated = false;
        }
        
        if (other.gameObject.tag == "TriggerVolume3")
        {
            GameMaster.main.player3Activated = false;
        }
        
        if (other.gameObject.tag == "TriggerVolume4")
        {
            GameMaster.main.player4Activated = false;
        }
    }
}
