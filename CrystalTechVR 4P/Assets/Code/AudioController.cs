﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioController : MonoBehaviour
{
	public static AudioController instance;

	public AudioSource mainAudio;

	private void Awake()
	{
		instance = this;

		if (!mainAudio)
		{
			mainAudio = GetComponent<AudioSource>();
		}
	}


}
