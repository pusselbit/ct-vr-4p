﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
[CreateAssetMenu(fileName = "New Tarot Card", menuName = "CrystalTech/Create New Spirit Animal", order = 2)]

public class SpiritAnimal : ScriptableObject
{
	[SerializeField]
	public string animalName;
	[SerializeField]
	public AudioClip sound1;
}
