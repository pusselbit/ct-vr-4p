Shader "Tauri Interactive/Advanced Glow PBS (2 pass)" 
  {
    Properties 
    {
     _MainColor ("Main Color", Color) = (0.227,0.227,0.612,1)
     _MainTex ("Texture", 2D) = "white" {}
     _BumpMap ("Bumpmap", 2D) = "bump" {}
     _Metallic ("Metallic", 2D) = "black" {}
     _GlowColor ("Glow Color", Color) = (0,0.38,1,1)
     _GlowPower ("Glow Power", Range(10,0.5)) = 10
     _Albedo ("Albedo", Range(0,1)) = 0
     _Emission("Emission", Range(1,3)) = 1.3
    }
    
    SubShader 
    {
     Tags 
     { 
     	"Queue"="Transparent" "IgnoreProjector"="True" "RenderType"="Transparent"
     }
     		
     
     CGPROGRAM
      #pragma surface surf Standard alpha fullforwardshadows addshadow
	  #pragma target 3.0
      struct Input {
          float2 uv_MainTex;
          float2 uv_BumpMap;
          float3 viewDir;
      };
      float4 _MainColor;
      sampler2D _MainTex;
      sampler2D _BumpMap;
      sampler2D _Metallic;
      float4 _GlowColor;
      float _GlowPower;
      float _Albedo;
      float _Emission;
     
      void surf (Input IN, inout SurfaceOutputStandard o) {      
      	  float4 tex = tex2D (_MainTex, IN.uv_MainTex);
          o.Alpha = tex.a + _MainColor.a;
          o.Normal = UnpackNormal (tex2D (_BumpMap, IN.uv_BumpMap));
      	  o.Metallic = tex2D (_Metallic, IN.uv_MainTex).x;  
          o.Albedo = _Albedo * tex.rgb * _MainColor.rgb;
      	  o.Smoothness = tex2D (_Metallic, IN.uv_MainTex).a;
      }
      ENDCG
     

	 Tags 
     { 
     	"Queue"="Transparent" "IgnoreProjector"="True" "RenderType"="Transparent"
     }
     		
     
     CGPROGRAM
      #pragma surface surf Lambert alpha
	  #pragma target 3.0
      struct Input {
          float2 uv_MainTex;
          float2 uv_BumpMap;
          float3 viewDir;
      };
      float4 _MainColor;
      sampler2D _MainTex;
      sampler2D _BumpMap;
      sampler2D _Metallic;
      float4 _GlowColor;
      float _GlowPower;
      float _Albedo;
      float _Emission;
     
      void surf (Input IN, inout SurfaceOutput o) {      
          half rim = _Emission - saturate(dot (normalize(IN.viewDir), o.Normal));
          o.Alpha =  pow (rim, _GlowPower);
          o.Emission = _GlowColor.rgb * pow (rim, _GlowPower);		  
      }
      ENDCG
    } 
  Fallback "Diffuse"
} 